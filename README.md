This is a setup for writing code on mac osx.  It demos 

  o  swift,
    *  How to make an OSX app
    *  How to implement some basic algorithms for job interview questions
        ooo  Linked List
        ooo  Reversing a string
        ooo  Binary Tree
  o  objective-c 
    * How to call from swift
  o  C
    *  How to call c from swift
    *  Various exercises from R. Stevens "Unix Network Programming" Book
    *  Various exercises from R. Stevens "Advanced Unix Programming" Book
    
  o  generic unit tests and performance tests.  
  o  there is a stub for UI tests but none implmeneted at this time
  