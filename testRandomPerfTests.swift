//
//  testRandomPerfTests.swift
//  SimpleApp
//
//  Created by John F. Davis on 3/13/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

import XCTest
@testable import SimpleApp

class SimpleAppRandomPerfTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testPerformanceCStyleForLoop() {
        print("****** This is a performance test for a c style for loop ********");
        self.measureBlock {
            // How long does it take to add 1000 items by odd counts using a C-style for loop

            var sum : Int;
            var i : Int;
            sum = 0;
            for (i=0; i<1000000; i++) {
                if ( i % 2 == 0 ) {
                    sum = sum + i;
                }
            }
            print("sum = ", sum);
        }
    }

    func testPerformanceSwiftStyleForLoop() {
        print("****** This is a performance test for a swift style for loop ********");
        self.measureBlock {
            // How long does it take to add 1000 items by odd counts using a C-style for loop
            
            var sum : Int;
            sum = 0;
            for i in 0.stride(to:1000000, by: 2) {
                sum = sum + i;
            }
            print("sum = ", sum);
        }
    }

    
}
