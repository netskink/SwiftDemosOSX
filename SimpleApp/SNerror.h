//
//  error.h
//  SimpleApp
//
//  Created by John F. Davis on 3/13/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#ifndef error_h
#define error_h


void err_sys(const char *fmt, ...);

// Fatal error unrelated to system call
// print message and terminate

void err_quit(const char *fmt, ...);



#endif /* error_h */
