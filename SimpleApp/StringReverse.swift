//
//  StringReverse.swift
//  SimpleApp
//
//  Created by John Fred Davis on 3/14/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

import Foundation

func SNStringReverse(inString: String) -> String {
    
    var characters = Array(inString.characters);
    
    var begin : Int = 0;
    var end : Int = characters.count - 1;
    var temp : Character;
    
    while (end > begin) {
        temp = characters[begin];
        characters[begin] = characters[end];
        characters[end] = temp;
        end--;
        begin++;
    }
    
    return String(characters);
}
