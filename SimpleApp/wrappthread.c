//
//  wrappthread.c
//  SimpleApp
//
//  Created by John F. Davis on 3/14/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#include "unp.h"
#include "SNerror.h"
#include "wrappthread.h"

void Pthread_mutex_lock(pthread_mutex_t *mptr) {

    int n;
    
    if ( (n = pthread_mutex_lock(mptr)) == 0) {
        return;
    }
    errno = n;;
    err_sys("pthread_mutex_lock error");
}
