//
//  unp.h
//  SimpleApp
//
//  Created by John F. Davis on 3/13/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#ifndef unp_h
#define unp_h

#include "myconfig.h"           // my hand written config.h

#include <sys/types.h>          // basic system data types
#include <sys/socket.h>         // basic socket definitions
#include <sys/time.h>           // timeval{} for select()
// stuff omitted
#include <netinet/in.h>         // sockaddr_in{} and other internet defns
#include <arpa/inet.h>          // inet(3) functions, inet_pton(),

#include <errno.h>

#include <stdio.h>

// Stuff omitted - from line 20 start
#include <string.h>
#include <sys/stat.h>       // For S_xxx file mode constants
#include <sys/uio.h>        // for iovect{} and readv/writev
#include <unistd.h>         // for read()

// Stuff omitted - from line 53 start
#include <pthread.h>



// Stuff omitted - from line 140 start
#define MAXLINE ( (unsigned long) 4096)        // max text line length
#define BUFFSIZE ( (unsigned long)   8192)    // buffer size for reads and writes


// Stuff omitted - from line 147 start
// Following shortens all the typecasts of pointer arguments
#define SA struct sockaddr


#endif /* unp_h */
