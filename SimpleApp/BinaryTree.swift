//
//  BinaryTree.swift
//  SimpleApp
//
//  Created by John F. Davis on 3/13/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

import Foundation

class SNBTNode {
    
    // The convention is that the left is always <= to right
    
    var left : SNBTNode?
    var right : SNBTNode?
    
    var value : Int;
    
    init() {
        left = nil
        right = nil
        value = 0
    }
    
}

class SNBinaryTree {
    
    var root : SNBTNode?
    
    init() {
        root = nil;
    }
    
    func add(value: Int) {
        var parent : SNBTNode?
        var working : SNBTNode?
        
        parent = root
        working = nil
    
        while (parent != nil) {
            working = parent
            if (value <= working!.value) {
                parent = working!.left
            } else {
                parent = working!.right
                
            }
        }
        // At this point,
        // parent = nil
        // working = parent where new node is placed, or nil if empty tree

        // Handle the empty tree case
        if (working == nil) {
            working = SNBTNode()
            working?.value = value
            root = working
            return
        }

        // Handle the non empty tree
        if (value <= working!.value) {
            working!.left = SNBTNode()
            working!.left!.value = value
        } else {
            working!.right = SNBTNode()
            working!.right!.value = value
        }
    }
    
    // In order does left subtree, then prints value,  then does the right subtree
    func dumpInOrder(parent: SNBTNode?) {
        
        if (nil == parent) {
            print("Empty Node");
            print("Nothing to do");
            return
        }
        dumpInOrder(parent!.left);
        print("value = ", parent!.value);
        dumpInOrder(parent!.right);
    }

    
    
    func min(var parent: SNBTNode?) -> Int {
        var value : Int

        value = 0; // Assume a min value of zero

        while (nil !== parent) {
            value = parent!.value;
            parent = parent!.left
        }
        
        return value
    }

    func max(var parent: SNBTNode?) -> Int {
        var value : Int
        
        value = 0; // Assume a max value of zero
        
        while (nil !== parent) {
            value = parent!.value;
            parent = parent!.right
        }
        
        return value
    }
    
}



