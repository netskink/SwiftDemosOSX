//
//  CStringArray.swift
//  SimpleApp
//
//  Created by John Fred Davis on 3/14/16.
//  I copied this from https://gist.github.com/neilpa/b430d148d1c5f4ae5ddd
//
//
// This did not work for me.  I changed his code to use var instead of let and it
// complained in the usage.  here is what worked
// 
// This is from mikeash on irc in swift-lang
//
//import Glibc
//
// https://swiftlang.ng.bluemix.net/#/repl/ba18bcd0c530ef268f369aec5e38c812be5b9241ee91ff3d6a4094220041524b
//
//func f(p: UnsafeMutablePointer<UnsafeMutablePointer<Int8>>) {
//    var cursor = p
//    while cursor.memory != nil {
//        print(String.fromCString(cursor.memory))
//        cursor += 1
//    }
//}
//
//let strings = ["hello", "world"]
//var ptrs = strings.map({ strdup($0) })
//ptrs.append(nil)
//f(&ptrs)
//for ptr in ptrs {
//    free(ptr)
//}


import Foundation

// Usage

//let argv = CStringArray(["ls", "/"])
//posix_spawnp(nil, argv.pointers[0], nil, nil, argv.pointers, nil)

// Is this really the best way to extend the lifetime of C-style strings? The lifetime
// of those passed to the String.withCString closure are only guaranteed valid during
// that call. Tried cheating this by returning the same C string from the closure but it
// gets dealloc'd almost immediately after the closure returns. This isn't terrible when
// dealing with a small number of constant C strings since you can nest closures. But
// this breaks down when it's dynamic, e.g. creating the char** argv array for an exec
// call.
class CString {
    private let _len: Int
    let buffer: UnsafeMutablePointer<Int8>
    
    init(_ string: String) {
        (_len, buffer) = string.withCString {
            let len = Int(strlen($0) + 1)
            let dst = strcpy(UnsafeMutablePointer<Int8>.alloc(len), $0)
            return (len, dst)
        }
    }
    
    deinit {
        buffer.dealloc(_len)
        // Alternative approach
        //buffer.dealloc(Int(strlen(buffer) + 1))
    }
}

// An array of C-style strings (e.g. char**) for easier interop.
class CStringArray {
    // Have to keep the owning CString's alive so that the pointers
    // in our buffer aren't dealloc'd out from under us.
    private let _strings: [CString]
    var pointers: [UnsafeMutablePointer<Int8>]
    
    init(_ strings: [String]) {
        _strings = strings.map { CString($0) }
        pointers = _strings.map { $0.buffer }
        // NULL-terminate our string pointer buffer since things like
        // exec*() and posix_spawn() require this.
        pointers.append(nil)
    }
}