//
//  wrapsock.c
//  SimpleApp
//
//  Created by John F. Davis on 3/14/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//
#include "unp.h"
#include "SNerror.h"
#include "wrapsock.h"

int Socket(int family, int type, int protocol) {

    int n;
    
    if ( (n= socket(family, type, protocol)) < 0) {
        err_sys("socket error");
    }
    return n;
}