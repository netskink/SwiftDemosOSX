//
//  daytimeclient.c
//  SimpleApp
//
//  Created by John F. Davis on 3/13/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#include "unp.h"
#include "wrapsock.h"
#include "SNUtils.h"
#include "SNerror.h"
#include "daytimeclient.h"


// Code from Stevens book
// macos 10.x does not have a daytime server, so this will use one at NIST


int doDaytime0(void) {
    int argc = 2;
    // The code is not currently set to do hostname lookup.
    // The timeserver hostname is time-nw.nist.gov
    // The timerserver ip is 131.107.13.100
    char *argv[2] = {"doDayTime", "131.107.13.100"};
    return doDaytime(argc, argv);

}


int doDaytime(int argc, char *argv[]) {
    
    int sockfd;
    long int n;
    char recvline[MAXLINE + 1];
    struct sockaddr_in servaddr;
    
    if (argc !=2) {
        err_quit("usage: a.out <IPaddress>");
    }

    // Using the Socket function call instead of the other code
    sockfd = Socket(AF_INET, SOCK_STREAM, 0);
//    if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
//        err_sys("socket error");
//    }
    
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(13);      // daytime server
    // deviation
    int iRC = inet_pton(AF_INET, argv[1], &servaddr.sin_addr);
    if (iRC <= 0) {
        printf("inet_pton rc = %d\n", iRC);
        err_quit("inet_pton error for %s", argv[1]);
    }

    
    if (connect(sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0) {
        err_sys("connection error");
    }
    
    while ( (n = read(sockfd, recvline, MAXLINE)) > 0 ) {
        recvline[n] = 0;        // null terminate
        if (fputs(recvline, stdout) == EOF) {
            err_sys("fputs error");
        }
    }
    if (n < 0) {
        err_sys("read error");
    }
    return(1);

}




