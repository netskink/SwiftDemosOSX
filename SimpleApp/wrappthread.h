//
//  wrappthread.h
//  SimpleApp
//
//  Created by John F. Davis on 3/14/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#ifndef wrappthread_h
#define wrappthread_h

void Pthread_mutex_lock(pthread_mutex_t *mptr);


#endif /* wrappthread_h */
