//
//  wrapsock.h
//  SimpleApp
//
//  Created by John F. Davis on 3/14/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#ifndef wrapsock_h
#define wrapsock_h

int Socket(int family, int type, int protocol);


#endif /* wrapsock_h */
