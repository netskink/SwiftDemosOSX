//
//  error.c
//  SimpleApp
//
//  Created by John F. Davis on 3/13/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

// This is from stevens unix network programming book

#include "unp.h"
#include <stdarg.h>
#include <syslog.h>

#include "SNUtils.h"    // Added to get straight objective c with on object


int daemon_proc;        // set nonzero by daemon_init()


// All these routines call this private routine
static void err_doit(int, int, const char *, va_list);

// When an error occurs in a Unix function, the global variable errno is set to a positive number
// indicating the type of error and the function normally returns -1.

// Fatal error related to system call
// print message and terminate
void err_sys(const char *fmt, ...) {
    
    va_list ap;
    
    va_start(ap, fmt);
    err_doit(1, LOG_ERR, fmt, ap);
    va_end(ap);
    SNMyExit(1);
}


// Fatal error unrelated to system call
// print message and terminate

void err_quit(const char *fmt, ...) {
    va_list ap;
    
    va_start(ap,fmt);
    err_doit(0, LOG_ERR, fmt, ap);
    va_end(ap);
    SNMyExit(1);
}


// Print message and return to caller
// caller specifies "errnoflag" and "level"

static void err_doit(int errnoflag, int level, const char *fmt, va_list ap) {
    
    int errno_save;
    unsigned long n;    // DEVIATION, on x86_64 strlen returns an unsigned long
    char buf[MAXLINE + 1];
    
    errno_save = errno; // value caller might want printed
    
    vsnprintf(buf, MAXLINE, fmt, ap); // safe
    
    n = strlen(buf);

    if (errnoflag) {
        snprintf(buf + n, MAXLINE - n, ": %s", strerror(errno_save));
    }
    strcat(buf,"\n");
    
    if (daemon_proc) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-security"
        syslog(level, buf);
#pragma clang diagnostic push
    } else {
        fflush(stdout);     // in case stdout and stderr are the same
        fputs(buf, stderr);
        fflush(stderr);
    }
    
    return;

}