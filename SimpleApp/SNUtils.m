//
//  SNUtils.m
//  SimpleApp
//
//  Created by John F. Davis on 3/13/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SNUtils.h"

int SNMyExit(int value) {
    
    NSLog(@"Exit Called");
    
    // a stack overflow post mentioned [NSApp terminate:self];
    exit(value);
}
