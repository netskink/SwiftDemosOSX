//
//  LinkedList.swift
//  SimpleApp
//
//  Created by John Fred Davis on 3/10/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

import Foundation

class LLNode<T : Comparable> {
    var value : T?;
    var next : LLNode?;
    
    init() {
        value = nil;
        next = nil;
    }
}


// A singly linked list
//
class LinkedList<T : Comparable> {
    var head : LLNode<T>?;
    var tail : LLNode<T>?;
    
    init() {
        head = nil;
        tail = nil;
    }
    
    func append(value: T) {
        
        // The first time head and tail are nil.
        if (head == nil) {
            head = LLNode();
            tail = head;
            head!.value = value;
            // The next LLNode is already set to nil
        } else {
            tail!.next = LLNode();
            tail = tail!.next
            tail!.value = value;
        }
    }
    
    func dump() {
        if (head == nil) {
            print("List is empty. Nothing to do.");
            return;
        }
        
        var aLLNode : LLNode<T>?
        aLLNode = head
        while ( aLLNode != nil) {
            print("Value is ", aLLNode!.value!);
            aLLNode = aLLNode!.next;
        }
    }


    func count() -> Int {
        if (head == nil) {
            return 0;
        }
        
        var count : Int;
        var aLLNode : LLNode<T>?
        aLLNode = head
        count = 0;
        while ( aLLNode != nil) {
            count++;
            aLLNode = aLLNode!.next;
        }
        return count;
    }

    func reverse() {
        // Three ptrs to LLNodes
        
        // p iterates from the first LLNode until nil 
        var p : LLNode<T>?
        
        // q iterates until the last LLNode, consider it the current
        // LLNode
        var q : LLNode<T>?
        
        // r iterates up until the next to the last LLNode, consier it the previous LLNode
        // It is used to build reverse list
        var r : LLNode<T>?
        
        p = head;
        q = nil;
        r = nil;
        
        // Reverse the tail
        tail = head
        while(p != nil) {
            q = p
            p = p!.next
            q!.next = r
            r = q
        }
        
        // Assign the new head
        head = q
    }
    
    func delete(aLLNode : LLNode<T>) {
     
        // Even if the LLNode exists in the list, we need to find the previous one
        // so that we can remove this one and set the prior LLNode to point to
        // this LLNodes sucessor
        
        //if (aLLNode == nil) {
        //    // there is nothing to do
        //    return
        //}
        
        // maintain three pointers, prev, current and next.
        // p iterates from the first LLNode until nil or the designated LLNode
        var p : LLNode<T>?
        
        // q iterates until the last LLNode, consider it the current
        //  LLNode
        var q : LLNode<T>?
        // The previous LLNode.
        var r : LLNode<T>?

        
        // The LLNode was the head
        if (aLLNode === head) {
            head = aLLNode.next  // or head.next
            return
        }
        
        
        p = head;
        q = nil;
        
        while(p != nil) {
            r = q  // Set previous
            q = p  // set current
            p = p!.next // set next
            
            // Look until the current is the given LLNode
            if (q === aLLNode) {
                break;
            }
        }
        
        // If we found it, q is the LLNode we want to delete
        // If we went through the entire list, q will
        // not be the desired LLNode
        if (q === aLLNode) {
            r!.next = p
        }
        
        
        // Since I maintain a tail pointer for fast appends
        // I need to adjust tail in case its a removal of the tail LLNode
        if (q === tail) {
            tail = r
        }
        
    }
    
    // Find max
    func max() -> T? {
        
        var value : T;
    

        var p: LLNode<T>?
        p = head
        if (p == nil) {
            // empty list
            return nil
        } else {
            value = p!.value!
        }
        while (p != nil) {
            //            if ( isEqual(p!.value!, value) ) {
            if (p!.value > value) {
                value = p!.value!
            }
            p = p!.next
        }

        return value
    }
    
    
    // Find a LLNode with the specified value. Return nil if not found
    func find(value: T) -> LLNode<T>? {
        
        var p: LLNode<T>?
        p = head
        
        while (p != nil) {
//            if ( isEqual(p!.value!, value) ) {
            if (p!.value == value) {
                break
            }
            p = p!.next
        }
        
        return p
    }
    
}






