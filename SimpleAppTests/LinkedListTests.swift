//
//  LinkedListTests.swift
//  SimpleApp
//
//  Created by John F. Davis on 3/13/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

import XCTest
@testable import SimpleApp

class LinkedListTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }


    func testDumpingAnEmptyList() {
        print("****** This is a test to see what happens when we dump an empty list ********");
        
        // Try to dump an empty list
        let linkedList = LinkedList<Int>();
        linkedList.dump();
        
    }

    func testAddFourItemsAndDump() {
        print("****** This is test to see what happens when we dump a list with four items ********");
        
        // Create a list with four items and dump it.
        let linkedList = LinkedList<Int>();
        for (var i=0; i<4; i++) {
            linkedList.append(i);
        }
        linkedList.dump();
    }


    func testCount() {
        print("****** This is test to see what happens when we count the number of items in a list of ten items ********");
        
        // Create a list with four items and dump it.
        let linkedList = LinkedList<Int>();
        let count = 10;
        for (var i=0; i<count; i++) {
            linkedList.append(i);
        }
        XCTAssert(linkedList.count() == count, "Count result was not what was expected");
    }


    func testReverse() {
        print("****** This is test to see what happens when we count the number of items in a list of ten items ********");
        
        // Create a list with four items and dump it.
        let linkedList = LinkedList<Int>();
        let count = 10;
        for (var i=0; i<count; i++) {
            linkedList.append(i);
        }
        print("Dump the starting list");
        linkedList.dump();
        
        print("Dump the reverse");
        linkedList.reverse();
        linkedList.dump();
        
        XCTAssert(linkedList.count() == count, "Count result was not what was expected");
    }

    func testFind() {
        
        // Create a list and see if we can find all the odds.
        let linkedList = LinkedList<Int>();
        let count = 10;
        for (var i=0; i<count; i++) {
            linkedList.append(i);
        }
        
        // See if we can find all the odds
        print("Find all the odds");
        var aLLNode : LLNode<Int>?
        for (var i=0; i<count; i++) {
            if (i % 2 == 0) {
                aLLNode = linkedList.find(i);
                if (aLLNode != nil) {
                    print("Found ", aLLNode!.value);
                }
            }
        }
        
        // See if we can find all the evens
        print("Find all the evens");
        for (var i=0; i<count; i++) {
            if (i % 2 == 1) {
                aLLNode = linkedList.find(i);
                if (aLLNode != nil) {
                    print("Found ", aLLNode!.value);
                }
            }
        }
        
        
    }

    func testDelete() {
        print("****** This is test to see what happens when we count the number of items in a list of ten items ********");
        
        // Create a list with ten items and dump it.
        let linkedList = LinkedList<Int>();
        var count = 10;
        var i:Int
        for (i=0; i<count; i++) {
            linkedList.append(i);
        }
        print("Dump the starting list");
        linkedList.dump();
        
        print("Remove head and dumpe");
        linkedList.delete(linkedList.head!);
        linkedList.dump();
        XCTAssert(linkedList.count() == count - 1, "Count result was not what was expected");
        
        print("Remove tail and dumpe");
        linkedList.delete(linkedList.tail!);
        linkedList.dump();
        XCTAssert(linkedList.count() == count - 2, "Count result was not what was expected");
        
        // Since we have removed the head and tail, make sure they point the correct locations
        XCTAssert(linkedList.head!.value == 1, "Head result was not what was expected");
        XCTAssert(linkedList.tail!.value == 8, "Tail result was not what was expected");
        
        // Now test a delete of all even values
        //
        // ************************************ Demo's a swift way for doing a for loop
        //
        print("delete all the evens");
        count = linkedList.count();
        var aLLNode : LLNode<Int>?
        for i in 0.stride(through: count, by: 2) {
            if let foundLLNode = linkedList.find(i) {
                linkedList.delete(foundLLNode)
            }
            
        }
        print("See what we have left");
        linkedList.dump()
        XCTAssert(linkedList.count() == 4, "Count result was not what was expected");
        
        // Now test a delete of all odd values
        print("delete all the odds");
        // The count could be less than the max value in the list at this point
        // so as a hack, look for all the numbers up to max and delete all the odd ones.
        var value : Int;
        if (nil != linkedList.max()) {
            // The list could be empty.  In that case max returns nil.
            value = linkedList.max()!
        } else {
            value = 0
        }
        count = value
        for (i=0; i<=count; i++) {
            print("i in loop",i);
            if (i % 2 == 1) {
                print("i which is odd",i);
                aLLNode = linkedList.find(i);
                if (aLLNode != nil) {
                    linkedList.delete(aLLNode!);
                }
            }
        }
        print("See what we have left");
        linkedList.dump()
        XCTAssert(linkedList.count() == 0, "Count result was not what was expected");
        
    }

}