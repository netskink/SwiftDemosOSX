//
//  testStringReverse.swift
//  SimpleApp
//
//  Created by John Fred Davis on 3/14/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

import XCTest
@testable import SimpleApp

class StringReverseTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func testStringReverse() {
        print("****** What happens when we try to reverse a swift string ********");

        let string = "Hello";
        let reverse =  SNStringReverse(string);
        print("Reversed is", reverse);

    }
    
    
    
}
