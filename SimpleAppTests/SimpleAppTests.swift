//
//  SimpleAppTests.swift
//  SimpleAppTests
//
//  Created by John Fred Davis on 3/10/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

import XCTest
@testable import SimpleApp

class SimpleAppTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        print("****** This is a performance test ********");
        self.measureBlock {
            // How long does it take to create a linked list with 100 items.
            // Create a list with four items and dump it.
            let linkedList = LinkedList<Int>();
            for (var i=0; i<100; i++) {
                linkedList.append(i);
            }
            
        }
    }
    
}
