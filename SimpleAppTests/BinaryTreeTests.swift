//
//  BinaryTreeTests.swift
//  SimpleApp
//
//  Created by John F. Davis on 3/13/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

import Foundation

import XCTest
@testable import SimpleApp

class BinaryTreeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testDumpEmptyBinaryTree() {
        print("****** This is a test to see what happens when we dump an empty binary tree ********");
        
        // Try to dump an empty list
        let binaryTree = SNBinaryTree();
        binaryTree.dumpInOrder(binaryTree.root);
        
    }

    
    func testAddAndDumpSmallBinaryTree0() {
        print("****** This is a test to see what happens when we dump a 1,2,3 binary tree ********");
        
        // Try to dump an empty list
        let binaryTree = SNBinaryTree();
        binaryTree.add(1);
        binaryTree.add(2);
        binaryTree.add(3);
        binaryTree.dumpInOrder(binaryTree.root);
        
    }

    
    func testAddAndDumpSmallBinaryTree1() {
        print("****** This is a test to see what happens when we dump a 3,2,1 binary tree ********");
        
        // Try to dump an empty list
        let binaryTree = SNBinaryTree();
        binaryTree.add(3);
        binaryTree.add(2);
        binaryTree.add(1);
        binaryTree.dumpInOrder(binaryTree.root);
        
    }

    
    func testAddAndDumpSmallBinaryTree2() {
        print("****** This is a test to see what happens when we dump a random binary tree ********");
        
        // Try to dump an empty list
        let binaryTree = SNBinaryTree();
        binaryTree.add(8);
        binaryTree.add(2);
        binaryTree.add(9);
        binaryTree.add(4);
        binaryTree.add(3);
        binaryTree.add(8);
        binaryTree.add(1);
        binaryTree.add(10);
        binaryTree.add(6);
        binaryTree.add(5);
        binaryTree.dumpInOrder(binaryTree.root);
        
    }

    
    func testMinAndMaxOfBinaryTree() {
        print("****** This is a test to see what happens when we find min and max of a binary tree ********");
        
        // Try to dump an empty list
        let binaryTree = SNBinaryTree();
        binaryTree.add(8);
        binaryTree.add(2);
        binaryTree.add(9);
        binaryTree.add(4);
        binaryTree.add(3);
        binaryTree.add(8);
        binaryTree.add(1);
        binaryTree.add(10);
        binaryTree.add(6);
        binaryTree.add(5);

        let min : Int = 1
        let max : Int = 10
        
        XCTAssertTrue(min == binaryTree.min(binaryTree.root), "Failed to find min");
        XCTAssertTrue(max == binaryTree.max(binaryTree.root), "Failed to find max");
    }

    
    
}
