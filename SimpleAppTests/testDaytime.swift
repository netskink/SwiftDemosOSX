//
//  testDaytime.swift
//  SimpleApp
//
//  Created by John F. Davis on 3/13/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//


import XCTest
@testable import SimpleApp

class DaytimeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testDaytime() {
        print("****** What happens when we try to call some c network code ********");
        
        let argc : Int32 = 2;

        // make the argv
        let strings = ["dodaytimecouldbeanything","131.107.13.100"]
        var ptrs = strings.map({strdup($0) })
        
        
        doDaytime(argc, &ptrs);
        
        for ptr in ptrs {
            free(ptr)
        }
        
        
    }
    
    
    
}
